resource "aws_security_group" "alb_to_workers_nodeport" {
  name_prefix = "alb_to_workers_nodeport"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "TCP"
    from_port   = 30000
    to_port     = 32767
    cidr_blocks = local.public_subnets
  }

  egress {
    protocol    = "TCP"
    from_port   = 30000
    to_port     = 32767
    cidr_blocks = local.public_subnets
  }

  tags = local.tags
}

resource "aws_security_group" "bastion_to_workers_ssh" {
  name_prefix = "bastion_to_workers_ssh"
  vpc_id      = module.vpc.vpc_id

  ingress {
    protocol    = "TCP"
    from_port   = 22
    to_port     = 22
    cidr_blocks = local.public_subnets
  }

  tags = local.tags
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "12.2.0"

  cluster_name           = local.cluster_name
  cluster_version        = var.cluster_version
  worker_ami_name_filter = var.worker_ami_name_filter
  workers_role_name      = local.workers_role_name
  subnets                = module.vpc.private_subnets
  vpc_id                 = module.vpc.vpc_id
  enable_irsa            = true # OpenID provider for IAM Roles as Service Accounts

  worker_groups_launch_template = local.spot_workers
  worker_additional_security_group_ids = [
    aws_security_group.bastion_to_workers_ssh.id,
    aws_security_group.alb_to_workers_nodeport.id,
  ]

  cluster_enabled_log_types = var.cluster_enabled_log_types

  tags = local.tags
}
