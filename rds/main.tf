terraform {
  required_version = "= 0.13.2"

  backend "s3" {
    bucket = "karo-eks-state"
    key    = "rds"
    region = "eu-west-1"
  }
}

provider "aws" {
  version = ">= 2.28.1"
  region  = var.region
}
