locals {
  availability_zones = ["${var.region}a", "${var.region}b", "${var.region}c"]

  engine            = "postgres"
  engine_version    = "12.3"
  instance_class    = "db.t2.micro"
  allocated_storage = 5
  port              = "5432"

  tags = {
  }
}
