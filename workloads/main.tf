# Very rudimentary config.
provider "helm" {
}

resource "helm_release" "drone" {
  name       = "drone"
  repository = "https://charts.drone.io"
  chart      = "drone"
  version    = "0.1.7"

  values = [
    file("values.yaml")
  ]
}
