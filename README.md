EKS IAC setup
=============

The EKS module manages to set up a private cluster pretty neatly.

The RDS tree sets up a Postgres in a seperate private VPC. Peering takes place in the cluster
("global") state. Databases are normally expected to outlive cluster.

The workloads tree is rather simplistic and just installs Drone. The values file is not in the repo
because it contains secrets.

Missing components, since that would take a lot more time:

- ALB ingress controller + SSL
- external DNS (we cheat by hardcoding the ELB address of the LoadBalancer type service)
- log forwarding
- monitoring agents
- external secrets
- node autoscaling
- spot termination handler
- SSH key management

State bucket
------------

```
BUCKET_NAME=karo-eks-state
aws s3api create-bucket --bucket "$BUCKET_NAME" --region eu-west-1  --create-bucket-configuration LocationConstraint=eu-west-1
aws s3api put-bucket-versioning --bucket "$BUCKET_NAME" --versioning-configuration Status=Enabled
aws s3api put-public-access-block --bucket "$BUCKET_NAME" --public-access-block-configuration BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true
```

Screenshot
----------

![Deployed Drone](deployed-workload.png)
