locals {
  cluster_name = coalesce(var.cluster_name, "cluster-${var.region}")
  availability_zones = coalesce(
    var.availability_zones,
    ["${var.region}a", "${var.region}b", "${var.region}c"]
  )

  vpc_cidr = "10.${var.vpc_second_octet}.0.0/16"
  private_subnets = coalesce(
    var.private_subnets,
    [
      "10.${var.vpc_second_octet}.64.0/18",
      "10.${var.vpc_second_octet}.128.0/18",
      "10.${var.vpc_second_octet}.192.0/18",
    ]
  )
  public_subnets = coalesce(
    var.public_subnets,
    [
      "10.${var.vpc_second_octet}.0.0/24",
      "10.${var.vpc_second_octet}.1.0/24",
      "10.${var.vpc_second_octet}.2.0/24",
    ]
  )
  bastion_subnet = module.vpc.public_subnets[0]

  workers_role_name = "${var.cluster_name}-workers"

  spot_workers = [
    for i in range(length(module.vpc.private_subnets)) :
    {
      name                    = "${local.workers_role_name}-spot-${local.availability_zones[i]}"
      key_name                = aws_key_pair.bastion.key_name
      subnets                 = [module.vpc.private_subnets[i]]
      override_instance_types = var.worker_instance_types
      spot_instance_pools     = length(var.worker_instance_types)
      ebs_optimized           = false
      asg_desired_capacity    = var.asg_desired_capacity
      asg_min_size            = var.asg_min_size
      asg_max_size            = var.asg_max_size
      autoscaling_enabled     = true
      tags = [
        {
          "key"                 = "k8s.io/cluster-autoscaler/node-template/label/node.kubernetes.io/lifecycle"
          "value"               = "spot"
          "propagate_at_launch" = "true"
        },
      ]
      kubelet_extra_args = "--node-labels=node.kubernetes.io/lifecycle=spot"
    }
  ]

  tags = {
    cluster = local.cluster_name
  }
}
