data "aws_subnet_ids" "rds_vpc" {
  vpc_id = module.vpc.vpc_id
}

data "aws_security_group" "rds_vpc" {
  vpc_id = module.vpc.vpc_id
  name   = "default"
}

module "drone-master" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "drone-master"

  engine            = local.engine
  engine_version    = local.engine_version
  instance_class    = local.instance_class
  allocated_storage = local.allocated_storage

  name     = "drone"
  username = "drone"
  password = "change_me_ill_be_stored_in_the_state_HvbIWYwleJgf9HJrp0U6RnrXq!"
  port     = local.port

  vpc_security_group_ids = [data.aws_security_group.rds_vpc.id]
  # Enable automatic failover:
  multi_az = true

  maintenance_window = "Sun:00:00-Sun:03:00"
  backup_window      = "03:00-06:00"

  # Backups are required in order to create a replica
  backup_retention_period = 7

  # DB subnet group
  subnet_ids = data.aws_subnet_ids.rds_vpc.ids

  create_db_option_group    = false
  create_db_parameter_group = false
}
