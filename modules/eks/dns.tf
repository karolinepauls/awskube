resource "aws_route53_zone" "cluster" {
  name          = var.cluster_domain
  force_destroy = true

  tags = local.tags
}

resource "aws_route53_record" "bastion" {
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "bastion.${var.cluster_domain}"
  type    = "A"
  ttl     = "300"

  records = [aws_instance.bastion-host.public_ip]
}

# This is cheating - normally this would be done by an ALB controller and external-dns.
resource "aws_route53_record" "drone" {
  zone_id = aws_route53_zone.cluster.zone_id
  name    = "drone.${var.cluster_domain}"
  type    = "CNAME"
  ttl     = "300"

  records = ["ab6401ece12c54ce1a959b896b35aba4-893349425.eu-west-1.elb.amazonaws.com"]
}
