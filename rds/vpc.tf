module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.47.0"

  name                 = "RDS VPC"
  cidr                 = "10.2.0.0/16"
  azs                  = local.availability_zones
  private_subnets      = var.private_subnets
  enable_nat_gateway   = false
  enable_dns_hostnames = true

  manage_default_security_group = true
  default_security_group_ingress = [{
    protocol    = "TCP"
    from_port   = local.port
    to_port     = local.port
    cidr_blocks = "10.0.0.0/8" # Allow all peered 10.x.x.x VPCs.
  }]
  default_security_group_egress = [{
    protocol    = "TCP"
    from_port   = local.port
    to_port     = local.port
    cidr_blocks = "10.0.0.0/8" # Allow all peered 10.x.x.x VPCs.
  }]

  tags = local.tags
}
