variable "region" {
  default = "eu-west-1"
}

variable "private_subnets" {
  default = [
    "10.2.64.0/18",
    "10.2.128.0/18",
    "10.2.192.0/18",
  ]
}
