terraform {
  required_version = "= 0.13.2"

  backend "s3" {
    bucket = "karo-eks-state"
    key    = "global"
    region = "eu-west-1"
  }
}

module "eks" {
  source                 = "../modules/eks"
  cluster_name           = "moo"
  worker_ami_name_filter = "amazon-eks-node-1.17-v20200904"
  ssh_key                = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC85KLYuVgiAO3oqGz/V3WhGHWoW91DchW6iz2qbwTka/B9ONFXHZD01R0zY7dP2lGTcsdsJE3qMPC4rIw2mD44hrxwthn2DRidu3MRnFy/Q60wp00Nfm3MCJHPidmTn+NOo2RfYwCJa0gV2yHLC+tR+D0Fe7rfGD/4/r6NHTc5wEM5NDRfgO+Zqn/yMXNlhDyY3HEYhz865dydCfPpuGmJgZn/wOUIS88xrAUZ/YwRHIgCXzzY71MtNUvQzwb+ScJ0Qt+7YHQbfAUVRVTeIJO+769+vNCavIUrUVEW18ZfmCW2w0XHkS5UI/WaDqRsoe1eeO2UeD+gZiVYjzskNPsopWmNMRRPoRPbhgrjb2uSgMQ2LXkwRN/MIvy5f9vjOvRBP+Q8Y6jqfJLSbQNFhglS5ZqZL1HgCtyLshFONI0irA9lGRCcXqmPaZmUWVCl+chGLbk0g76UTUUwtOq5AIPA3sors7FRDygNPi/newx3C8z+uzK57tRer9sZESp0EC8= karo@moohost"
  cluster_domain         = "eks.karolinepauls.com"
}
