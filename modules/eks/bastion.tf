data "aws_ami" "bastion" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.????????-x86_64-gp2"]
  }

  owners = ["137112412989"]
}

resource "aws_key_pair" "bastion" {
  key_name   = "${var.cluster_name}-bastion"
  public_key = var.ssh_key
}

resource "aws_security_group" "bastion" {
  name        = "${var.cluster_name}-bastion"
  description = "Allow SSH access to bastion host and SSH from the bastion to workers"
  vpc_id      = module.vpc.vpc_id

  # SSH from the internet to the bastion.
  ingress {
    protocol    = "TCP"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = local.tags
}

resource "aws_instance" "bastion-host" {
  instance_type          = "t2.micro"
  ami                    = data.aws_ami.bastion.id
  key_name               = aws_key_pair.bastion.key_name
  vpc_security_group_ids = [aws_security_group.bastion.id]
  subnet_id              = local.bastion_subnet

  associate_public_ip_address = true

  root_block_device {
    volume_size           = "10"
    delete_on_termination = true
  }

  tags = merge(local.tags, {
    Name = "${local.cluster_name}-bastion"
  })
}
