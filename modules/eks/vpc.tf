module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.47.0"

  name                   = local.cluster_name
  cidr                   = local.vpc_cidr
  azs                    = local.availability_zones
  public_subnets         = local.public_subnets
  private_subnets        = local.private_subnets
  enable_dns_hostnames   = true
  enable_nat_gateway     = true
  one_nat_gateway_per_az = true

  tags = merge(
    local.tags,
    {
      "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  })

  public_subnet_tags = merge(local.tags, {
    # For aws-alb-ingress-controller:
    "kubernetes.io/role/elb" = "1"
    # Shouldn't be needed past EKS 1.15:
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  })

  # kubernetes.io/role/internal-elb also used for finding which subnets to enable in VPC peering.
  private_subnet_tags = merge(local.tags, {
    "kubernetes.io/role/internal-elb" = "1"
    # Shouldn't be needed past EKS 1.15:
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  })
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "karo-eks-state"
    key    = "rds"
    region = "eu-west-1"
  }
}

data "aws_route_tables" "rds" {
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id
}

resource "aws_vpc_peering_connection" "rds" {
  peer_vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id
  vpc_id      = module.vpc.vpc_id
  auto_accept = true

  accepter {
    allow_remote_vpc_dns_resolution = true
  }

  requester {
    allow_remote_vpc_dns_resolution = true
  }
}

resource "aws_route" "to_rds" {
  for_each = toset(concat(module.vpc.public_route_table_ids, module.vpc.private_route_table_ids))

  route_table_id            = each.key
  destination_cidr_block    = data.terraform_remote_state.vpc.outputs.vpc_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}

resource "aws_route" "from_rds" {
  for_each = toset(data.aws_route_tables.rds.ids)

  route_table_id            = each.key
  destination_cidr_block    = module.vpc.vpc_cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.rds.id
}
