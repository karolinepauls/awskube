variable "region" {
  default = "eu-west-1"
}

variable "cluster_version" {
  default = "1.17"
  type    = string
}

variable "cluster_name" {
  default = null
  type    = string
}

variable "cluster_domain" {
  type = string
}

variable "availability_zones" {
  default = null
  type    = list(string)
}

variable "vpc_second_octet" {
  default = "1"
  type    = string
}

variable "private_subnets" {
  default = null
  type    = list(string)
}

variable "public_subnets" {
  default = null
  type    = list(string)
}

variable "bastion_subnet" {
  default = null
  type    = string
}

variable "worker_ami_name_filter" {
  description = "Worker AMI name filter query. Make sure it returns a single worker."
  type        = string
}

variable "asg_min_size" {
  default = 0
  type    = number
}

variable "asg_max_size" {
  default = 5
  type    = number
}

variable "asg_desired_capacity" {
  default = 1
  type    = number
}

variable "worker_instance_types" {
  description = "Worker instance types. Should be similar in size."
  default     = ["t2.micro"]
  type        = list(string)
}

variable "cluster_enabled_log_types" {
  default     = []
  description = "E.g. [\"api\", \"audit\", \"authenticator\", \"controllerManager\", \"scheduler\"]"
  type        = list(string)
}

variable "ssh_key" {
  description = "Default SSH key."
}
